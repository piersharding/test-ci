# This Makefile describes a set of common tasks for managing the
# build environment and integrating with the CI/CD pipeline for the
#
# type: `make` to list the available commands.
#
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
BASEDIR := $(patsubst %/,%,$(dir $(MAKEPATH)))

# Fixed variables
TIMEOUT = 86400

# Docker and Gitlab CI variables
RDEBUG ?= ""
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
HOST ?= docker
DOCKER_HOST ?= tcp://$(HOST):2375
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker

PYTHON_BASE_IMAGE ?= python:3
TAG ?= $(shell echo $(PYTHON_BASE_IMAGE) | sed 's/\://')
IMAGE ?= piersharding/test-ci
EXAMPLE_HOST ?= example-test-ci-test
EXAMPLE_VOLUME ?= false
CLEAN_UP ?= --cleanup
S3_HOST ?= minio:9000
BUCKET ?= reports
REPORTS ?= reports
AWS_ACCESS_KEY ?=
AWS_SECRET_KEY ?=

#Helm version
HELM_VERSION = v2.14.0
# kubectl version
KUBERNETES_VERSION = v1.14.1

# HELM_CHART the chart name
HELM_CHART ?= test-ci
KUBE_NAMESPACE ?= default
HELM_RELEASE ?= test
INGRESS_HOST ?= test-ci.minikube.local
PORT ?= 80

# define overides for above variables in here
-include citools/tools.mak

# define personal overides for above variables in here
-include PrivateRules.mak
#
# Defines a default make target so that help is printed if make is called
# without a target
#
.DEFAULT_GOAL := help

.PHONY: check rcheck build push clean help run test rcheck2 show lint deploy delete logs describe mkcerts localip namespace rescue_reports

help:  ## show this help.
	@grep -hE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build:
	cp -r Makefile example.py tests reports build/
	cd build && \
	docker build \
	  --build-arg PYTHON_BASE_IMAGE=$(PYTHON_BASE_IMAGE) \
	  -t test-ci:latest -f Dockerfile .

push: build
	docker tag test-ci:latest $(IMAGE):$(TAG)
	docker push $(IMAGE):$(TAG)
	docker tag test-ci:latest $(IMAGE):latest
	docker push $(IMAGE):latest

clean: ## clean everything
	rm -rf *.xml reports/*.xml build/*.py build/reports build/tests

check: ## run pylint
	pylint3 example.py

run: ## run example
	python3 example.py

test: ## run unittest
	PYTHONPATH=./tests HOST=$(INGRESS_HOST) PORT=$(PORT) python3 -m xmlrunner TestBasic -o ./reports

test_volume:
	(echo "apiVersion: v1"; \
	 echo "kind: PersistentVolumeClaim"; \
	 echo "metadata:"; \
	 echo "  name: $(EXAMPLE_VOLUME)"; \
	 echo "spec:"; \
	 echo "  accessModes:"; \
	 echo "    - ReadWriteOnce"; \
	 echo "  storageClassName: standard"; \
	 echo "  resources:"; \
	 echo "    requests:"; \
	 echo "      storage: 1Gi") | kubectl -n $(KUBE_NAMESPACE) apply -f -

rescue_reports:
	 (echo "apiVersion: batch/v1"; \
		echo "kind: Job"; \
		echo "metadata:"; \
		echo "  name: test-report-rescue-$(EXAMPLE_VOLUME)"; \
		echo "spec:"; \
		echo "  template:"; \
		echo "    spec:"; \
		echo "      restartPolicy: Never"; \
		echo "      containers:"; \
		echo "      - name: rescue"; \
		echo "        image: piersharding/s3cmd:latest"; \
		echo "        imagePullPolicy: IfNotPresent"; \
		echo "        args:"; \
		echo "          - --host=$(S3_HOST)"; \
		echo "          - --host-bucket=$(S3_HOST)"; \
		echo "          - --no-ssl"; \
		echo "          - sync"; \
		echo "          - /app/reports/"; \
		echo "          - s3://$(BUCKET)/$(EXAMPLE_VOLUME)/"; \
		echo "        env:"; \
		echo "        - name: AWS_ACCESS_KEY"; \
		echo "          value: $(AWS_ACCESS_KEY)"; \
		echo "        - name: AWS_SECRET_KEY"; \
		echo "          value: $(AWS_SECRET_KEY)"; \
		echo "        volumeMounts:"; \
		echo "        - name: reports"; \
		echo "          mountPath: /app/reports"; \
		echo "      volumes:"; \
		echo "      - name: reports"; \
		echo "        persistentVolumeClaim:"; \
		echo "          claimName: $(EXAMPLE_VOLUME)") | kubectl -n $(KUBE_NAMESPACE) apply -f -
	kubectl -n $(KUBE_NAMESPACE) wait --for=condition=complete --timeout 30s job.batch/test-report-rescue-$(EXAMPLE_VOLUME)
	kubectl -n $(KUBE_NAMESPACE) delete job/test-report-rescue-$(EXAMPLE_VOLUME)

remove_test_volume:
	kubectl -n $(KUBE_NAMESPACE) delete pvc $(EXAMPLE_VOLUME)

pull_reports:
	# retrieve report files
	kubectl port-forward service/minio 9000 --namespace default &
	echo "Mounting in: $(BASEDIR)/$(REPORTS)"
	docker run --rm --net=host -i \
	  -e AWS_ACCESS_KEY=$(AWS_ACCESS_KEY) -e AWS_SECRET_KEY=$(AWS_SECRET_KEY) \
		--entrypoint=/bin/sh \
		piersharding/s3cmd:latest \
		-c '/opt/s3cmd/s3cmd  --host=$(S3_HOST) --host-bucket=$(S3_HOST) -q --no-ssl sync s3://$(BUCKET)/$(EXAMPLE_VOLUME)/ $(REPORTS)/ >/dev/null; tar -czf - $(REPORTS)' | tar -xzvf -

	# delete report directory and contents
	docker run --rm --net=host \
 	  -e AWS_ACCESS_KEY=$(AWS_ACCESS_KEY) -e AWS_SECRET_KEY=$(AWS_SECRET_KEY) \
 		 piersharding/s3cmd:latest \
 		 --host=$(S3_HOST) \
 		 --host-bucket=$(S3_HOST) \
 		 --no-ssl \
 		 del --recursive s3://$(BUCKET)/$(EXAMPLE_VOLUME)

pull_reports_no_object_storage:
	kubectl run --quiet -i --rm $(EXAMPLE_VOLUME) \
	  --overrides='{"spec": {"containers": [{"name": "rescue","image": "ubuntu:18.04","command": ["/bin/sh"], "args": ["-c", "cd /; tar -czf - $(REPORTS)"],"volumeMounts": [{"mountPath": "/$(REPORTS)","name": "reports"}]}],"volumes": [{"name":"reports","persistentVolumeClaim":{"claimName": "$(EXAMPLE_VOLUME)"}}]}}' \
		--image=ubuntu:18.04 --restart=Never | tar -xzvf -

rcheck:  ## run code standards check using gitlab-runner
	if [ -n "$(RDEBUG)" ]; then DEBUG_LEVEL=debug; else DEBUG_LEVEL=warn; fi && \
	gitlab-runner --log-level $${DEBUG_LEVEL} exec $(EXECUTOR) \
	--docker-privileged \
	 --docker-disable-cache=false \
	--docker-host $(DOCKER_HOST) \
	--docker-volumes  $(DOCKER_VOLUMES) \
	--docker-pull-policy always \
	--timeout $(TIMEOUT) \
    --env "DOCKER_HOST=$(DOCKER_HOST)" \
		--env "GITLAB_USER=$(GITLAB_USER)" \
		--env "REGISTRY_TOKEN=$(REGISTRY_TOKEN)" \
		--env "CI_BUILD_TOKEN=$(CI_BUILD_TOKEN)" \
		--env "TRACE=1" \
		--env "DEBUG=1" \
		--pre-build-script "apk add --update openssl curl tar gzip bash ca-certificates git; git clone https://github.com/piersharding/submake.git --branch master --single-branch --depth 1 ./citools" \
	check-code-standards || true

rcheck2:  ## run code standards flake8 check using gitlab-runner
		if [ -n "$(RDEBUG)" ]; then DEBUG_LEVEL=debug; else DEBUG_LEVEL=warn; fi && \
		gitlab-runner --log-level $${DEBUG_LEVEL} exec $(EXECUTOR) \
		--docker-privileged \
		 --docker-disable-cache=false \
		--docker-host $(DOCKER_HOST) \
		--docker-volumes  $(DOCKER_VOLUMES) \
		--docker-pull-policy always \
		--timeout $(TIMEOUT) \
	    --env "DOCKER_HOST=$(DOCKER_HOST)" \
			--env "GITLAB_USER=$(GITLAB_USER)" \
			--env "REGISTRY_TOKEN=$(REGISTRY_TOKEN)" \
			--env "CI_BUILD_TOKEN=$(CI_BUILD_TOKEN)" \
			--env "TRACE=1" \
			--env "DEBUG=1" \
			--pre-build-script "apk add --update openssl curl tar gzip bash ca-certificates git; git clone https://github.com/piersharding/submake.git --branch master --single-branch --depth 1 ./citools" \
		check-code-standards-flake8 || true

rtest:  ## run test using gitlab-runner
	if [ -n "$(RDEBUG)" ]; then DEBUG_LEVEL=debug; else DEBUG_LEVEL=warn; fi && \
	gitlab-runner --log-level $${DEBUG_LEVEL} exec $(EXECUTOR) \
		--docker-privileged \
		--docker-disable-cache=false \
		--docker-host $(DOCKER_HOST) \
		--docker-volumes  $(DOCKER_VOLUMES) \
		--docker-pull-policy always \
		--timeout $(TIMEOUT) \
    --env "DOCKER_HOST=$(DOCKER_HOST)" \
		--env "GITLAB_USER=$(GITLAB_USER)" \
		--env "REGISTRY_TOKEN=$(REGISTRY_TOKEN)" \
		--env "CI_BUILD_TOKEN=$(CI_BUILD_TOKEN)" \
		--env "KUBE_NAMESPACE=$(KUBE_NAMESPACE)" \
		--env "KUBECONFIG=$(KUBECONFIG)" \
		--env "KUBE_CONFIG_BASE64=$(KUBE_CONFIG_BASE64)" \
		--env "KUBERNETES_VERSION=$(KUBERNETES_VERSION)" \
		--env "HELM_VERSION=$(HELM_VERSION)" \
		--env "EXAMPLE_VOLUME=$(EXAMPLE_VOLUME)" \
		--env "S3_HOST=$(S3_HOST)" \
		--env "BUCKET=$(BUCKET)" \
		--env "REPORTS=$(REPORTS)" \
		--env "AWS_ACCESS_KEY=$(AWS_ACCESS_KEY)" \
		--env "AWS_SECRET_KEY=$(AWS_SECRET_KEY)" \
		--env "TRACE=1" \
		--env "DEBUG=1" \
	unit-tests || true

k8s: ## Which kubernetes are we connected to
	@echo "Kubernetes cluster-info:"
	@kubectl cluster-info
	@echo ""
	@echo "kubectl version:"
	@kubectl version
	@echo ""
	@echo "Helm version:"
	@helm version --client

namespace: ## create the kubernetes namespace
	kubectl describe namespace $(KUBE_NAMESPACE) || kubectl create namespace $(KUBE_NAMESPACE)

deploy: namespace mkcerts  ## deploy the helm chart
	@helm template charts/$(HELM_CHART)/ --name $(HELM_RELEASE) \
				 --namespace $(KUBE_NAMESPACE) \
         --tiller-namespace $(KUBE_NAMESPACE) \
				 --set example.hostname=$(EXAMPLE_HOST) \
				 --set helmTests=false \
				 --set ingress.hostname=$(INGRESS_HOST) | kubectl -n $(KUBE_NAMESPACE) apply -f -

show: mkcerts ## show the helm chart
	@helm template charts/$(HELM_CHART)/ --name $(HELM_RELEASE) \
				 --namespace $(KUBE_NAMESPACE) \
         --tiller-namespace $(KUBE_NAMESPACE) \
				 --set testingVolume=$(EXAMPLE_VOLUME) \
				 --set example.hostname=$(EXAMPLE_HOST) \
				 --set ingress.hostname=$(INGRESS_HOST)

lint: mkcerts ## lint check the helm chart
	@helm lint charts/$(HELM_CHART)/ \
				 --namespace $(KUBE_NAMESPACE) \
         --tiller-namespace $(KUBE_NAMESPACE) \
				 --set example.hostname=$(EXAMPLE_HOST) \
				 --set testingVolume=$(EXAMPLE_VOLUME) \
				 --set ingress.hostname=$(INGRESS_HOST)

install: namespace mkcerts  ## install the helm chart (with Tiller)
	@helm tiller run $(KUBE_NAMESPACE) -- helm install charts/$(HELM_CHART)/ --name $(HELM_RELEASE) \
				--wait \
				--namespace $(KUBE_NAMESPACE) \
				--tiller-namespace $(KUBE_NAMESPACE) \
				--set example.hostname=$(EXAMPLE_HOST) \
				--set testingVolume=$(EXAMPLE_VOLUME) \
				--set ingress.hostname=$(INGRESS_HOST)

helm_tests:  ## run Helm chart tests
	helm tiller run $(KUBE_NAMESPACE) -- helm test $(HELM_RELEASE) $(CLEAN_UP)

helm_delete: ## delete the helm chart release (with Tiller)
	@helm tiller run $(KUBE_NAMESPACE) -- helm delete $(HELM_RELEASE) --purge \
							 --tiller-namespace $(KUBE_NAMESPACE)

delete: ## delete the helm chart release
	@helm template charts/$(HELM_CHART)/ --name $(HELM_RELEASE) \
				 --namespace $(KUBE_NAMESPACE) \
         --tiller-namespace $(KUBE_NAMESPACE) \
				 --set example.hostname=$(EXAMPLE_HOST) \
				 --set helmTests=false \
				 --set ingress.hostname=$(INGRESS_HOST) | kubectl -n $(KUBE_NAMESPACE) delete -f -

describe: ## describe Pods executed from Helm chart
	@for i in `kubectl -n $(KUBE_NAMESPACE) get pods -l app.kubernetes.io/instance=$(HELM_RELEASE) -o=name`; \
	do echo "---------------------------------------------------"; \
	echo "Describe for $${i}"; \
	echo kubectl -n $(KUBE_NAMESPACE) describe $${i}; \
	echo "---------------------------------------------------"; \
	kubectl -n $(KUBE_NAMESPACE) describe $${i}; \
	echo "---------------------------------------------------"; \
	echo ""; echo ""; echo ""; \
	done

logs: ## show Helm chart POD logs
	@for i in `kubectl -n $(KUBE_NAMESPACE) get pods -l app.kubernetes.io/instance=$(HELM_RELEASE) -o=name`; \
	do \
	echo "---------------------------------------------------"; \
	echo "Logs for $${i}"; \
	echo kubectl -n $(KUBE_NAMESPACE) logs $${i}; \
	echo kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"`; do \
	RES=`kubectl -n $(KUBE_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "initContainer: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	echo "Main Pod logs for $${i}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.containers[*].name}"`; do \
	RES=`kubectl -n $(KUBE_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "Container: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	echo "---------------------------------------------------"; \
	echo ""; echo ""; echo ""; \
	done

localip:  ## set local Minikube IP in /etc/hosts file for Ingress $(INGRESS_HOST)
	@new_ip=`minikube ip` && \
	existing_ip=`grep $(INGRESS_HOST) /etc/hosts || true` && \
	echo "New IP is: $${new_ip}" && \
	echo "Existing IP: $${existing_ip}" && \
	if [ -z "$${existing_ip}" ]; then echo "$${new_ip} $(INGRESS_HOST)" | sudo tee -a /etc/hosts; \
	else sudo perl -i -ne "s/\d+\.\d+.\d+\.\d+/$${new_ip}/ if /$(INGRESS_HOST)/; print" /etc/hosts; fi && \
	echo "/etc/hosts is now: " `grep $(INGRESS_HOST) /etc/hosts`

mkcerts:  ## Make dummy certificates for $(INGRESS_HOST) and Ingress
	@if [ ! -f charts/$(HELM_CHART)/secrets/tls.key ]; then \
	openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 \
	   -keyout charts/$(HELM_CHART)/secrets/tls.key \
		 -out charts/$(HELM_CHART)/secrets/tls.crt \
		 -subj "/CN=$(INGRESS_HOST)/O=Minikube"; \
	else \
	echo "SSL cert already exits in charts/$(HELM_CHART)/secrets ... skipping"; \
	fi

# Utility target to install Helm dependencies
helm_dependencies:
	@which helm ; rc=$$?; \
	if [[ $$rc != 0 ]]; then \
	curl "https://kubernetes-helm.storage.googleapis.com/helm-$(HELM_VERSION)-linux-amd64.tar.gz" | tar zx; \
	mv linux-amd64/helm /usr/bin/; \
	helm init --client-only; \
	fi
	@helm init --client-only
	@if [ ! -d $$HOME/.helm/plugins/helm-tiller ]; then \
	echo "installing tiller plugin..."; \
	helm plugin install https://github.com/rimusz/helm-tiller; \
	fi
	helm version --client
	@helm tiller stop 2>/dev/null || true

# Utility target to install K8s dependencies
kubectl_dependencies:
	@([ -n "$(KUBE_CONFIG_BASE64)" ] && [ -n "$(KUBECONFIG)" ]) || (echo "unset variables [KUBE_CONFIG_BASE64/KUBECONFIG] - abort!"; exit 1)
	@which kubectl ; rc=$$?; \
	if [[ $$rc != 0 ]]; then \
		curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/$(KUBERNETES_VERSION)/bin/linux/amd64/kubectl"; \
		chmod +x /usr/bin/kubectl; \
		mkdir -p /etc/deploy; \
		echo $(KUBE_CONFIG_BASE64) | base64 -d > $(KUBECONFIG); \
	fi
	@echo -e "\nkubectl client version:"
	kubectl version --client
	@echo -e "\nkubectl config view:"
	@kubectl config view
	@echo -e "\nkubectl config get-contexts:"
	@kubectl config get-contexts
	@echo -e "\nkubectl version:"
	@kubectl version

kubeconfig: ## export current KUBECONFIG as base64 ready for KUBE_CONFIG_BASE64
	@KUBE_CONFIG_BASE64=`kubectl config view --flatten | base64 -w 0`; \
	echo "KUBE_CONFIG_BASE64: $$(echo $${KUBE_CONFIG_BASE64} | cut -c 1-40)..."; \
	echo "appended to: PrivateRules.mak"; \
	echo ""; \
	echo "# base64 encoded from: kubectl config view --flatten"; \
	echo "KUBE_CONFIG_BASE64 = $${KUBE_CONFIG_BASE64}" >> PrivateRules.mak
