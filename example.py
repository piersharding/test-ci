#!/usr/bin/env python
"""
sync is a minimal HTTP server that responds to POST requests from
the MetaController for the configuration of MPIJob objects in Kubernetes
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s [%(name)s] %(levelname)s: %(message)s')


class Controller(BaseHTTPRequestHandler):
    """
    Basic HHTP controller - handles only POST requests
    """

# we only handle POST requests
    def do_POST(self):  # pylint: disable=invalid-name
        """
        the POST responder...
        """

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps({'msg': 'hello'}).encode())

# we only handle POST requests
    def do_GET(self):  # pylint: disable=invalid-name
        """
        the GET responder...
        """

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps({'msg': 'hello'}).encode())


# boot the web server
HTTPServer(('', 8080), Controller).serve_forever()
