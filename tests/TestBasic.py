import unittest
import xmlrunner
import requests
import os


class TestBasic(unittest.TestCase):

    def setUp(self):
        self.host = (os.environ['HOST'] if 'HOST' in os.environ else 'localhost')
        self.port = (os.environ['PORT'] if 'PORT' in os.environ else '8080')
    def test_request_response(self):
        # Send a request to the API server and store the response.
        response = requests.get('http://%s:%s' % (self.host, self.port))

        # Confirm that the request-response cycle completed successfully.
        self.assertTrue(response.ok)

